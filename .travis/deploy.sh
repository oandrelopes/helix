#!/usr/bin/env bash

set -e

if [[ $TRAVIS_BRANCH == 'master' ]]; then
  DEPLOY_HOST=$DEPLOY_HOST_PROD
elif [[ $TRAVIS_BRANCH == 'staging' ]]; then
  DEPLOY_HOST=$DEPLOY_HOST_STAG
fi

echo "Packaging new release"
sbt clean debian:packageBin

DEB_FILE=$(ls target/helix*.deb | cut -d'/' -f 2)

echo "Copying new release to remote host"
scp target/$DEB_FILE $DEPLOY_USER@$DEPLOY_HOST:/tmp

echo "Installing update..."
ssh -t $DEPLOY_USER@$DEPLOY_HOST "sudo dpkg -i /tmp/$DEB_FILE"

echo "Done"
