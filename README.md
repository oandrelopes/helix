# helix [![Build Status](https://travis-ci.com/escaleseo/helix.svg?token=543idJdtsNSw2LFAgXjD&branch=development)](https://travis-ci.com/escaleseo/helix)

## About

Helix is Escale's mailing generator, as of right now it only processes Elektro data but it's intended to support other customers as well.

## Environment

This project uses Scala, this is how to have it running under a Unix-like environment.

### Installing dependencies

You'll need to have Java JDK and SBT installed in your system in order to have Helix running.
```
# This is how you install these under Arch-linux
sudo pacman -Sy sbt jdk8-openjdk

# This is how you install these under macOS (using brew)
brew install sbt
brew cask install java
```

In addition to that you will also need PostgreSQL:
```
# Arch linux
sudo pacman -Sy postgresql
sudo -u postgres -i
initdb --locale $LANG -E UTF8 -D '/var/lib/postgres/data'
createuser --interactive
createdb helix
exit
sudo systemctl start posgresql.service

# macOS
brew install postgresql
```

Last but not least, in order to build `.deb` packages you'll need: `dpkg-deb`, `dpkg-sig`, `dpkg-genchanges`, `lintian`, `fakeroot`

```
# macOS
brew install fakeroot dpkg
```

## Running

```
sbt run
```

## Testing

```
sbt test
```

## Deploying

This project has a CI proccess working on Travis that manipule our branch deploy

## Fixing I/O bugs 

> If you are having some trouble when trying to import Elektro mailing bases, you need to follow this steps to try to fix it... Have fun! 😁

1. First of all, you need to look at Elektro's email account `elektro.mailing@escale.com.br` the password is on your account at (LastPass)[http://lastpass.com]

2. With email account opened, look at sidebar there are 3 tags: 
  * *elektro*: Has all emails sent from elektro, contains info about new entries and PJ;
  * *elektro_done*: Has all emails imported in Helix's life;
  * *elektro_ownership*: : Has emails with ownership change info.
  
3. Check if **elektro** and **elektro_ownership** doesn't have any new email. If this answer been **"YESSSS, there is a new email"** follow step **4** or maybe this answer was **"NOOOO, God bless us"** you're save and can return to current job on Sprint 🤓.

4. Now with an email not imported, open a terminal and access Helix's instance with `escale.pem`
```
$ ssh -i ~/<YOUR ESCALE.PEM'S LOCATION> ubuntu@helix.escale.com.br
```

5. On terminal, you have to run Helix and read logs to identify what happened. (Most of times, elektro sent us an email with some information wrong, like an address null, a installation's date blank or in a String format):

```
Usage:
    helix [command]

Available commands are:

[import]
  - import-elektro-mailing
  - import-elektro-ownership

[send]
  - send-elektro-mailing
  - send-elektro-ownership
```

Helix command example on terminal:
```
$ helix import-elektro-mailing
```
