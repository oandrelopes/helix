CREATE TABLE elektro_mailing (
  indice INT,

  customer_number INT,
  customer_name VARCHAR(255),
  customer_email VARCHAR(255),
  customer_cpf_cnpj VARCHAR(20),
  customer_id_number VARCHAR(20),
  customer_birthdate DATE,

  uc_number INT,
  uc_address VARCHAR(255),
  uc_neighborhood VARCHAR(100),
  uc_city VARCHAR(100),
  uc_state VARCHAR(2),
  uc_zip_code VARCHAR(10),
  uc_zip_code_net_availability VARCHAR(50),
  uc_main_class VARCHAR(100),

  landline_phone VARCHAR(30),
  mobile_phone VARCHAR(30),
  requester_landline_phone VARCHAR(30),
  requester_mobile_phone VARCHAR(30),

  scheduled_mailing_date DATE,
  so_scheduled_installation_date DATE NOT NULL,
  so_type VARCHAR(255),
  so_locale_code VARCHAR(100),
  so_number VARCHAR(30) NOT NULL,
  so_status VARCHAR(50),

  PRIMARY KEY(so_number, so_scheduled_installation_date)
);
