package br.com.escale.helix.models

import java.util.Date

import com.typesafe.config.{Config, ConfigFactory}

case class ElektroMailingOwnershipRow(indice: Option[Int], customerNumber: Option[Int], customerName: String,
                                      customerEmail: Option[String], customerCpfCnpj: Option[String],
                                      customerIdNumber: Option[String], customerBirthdate: Option[Date], ucNumber: Int,
                                      ucAddress: String, ucNeighborhood: String, ucCity: String, ucState: String,
                                      ucZipCode: String, ucZipCodeNetAvailability: Option[String], ucMainClass: Option[String],
                                      landlinePhone: Option[String], mobilePhone: Option[String],
                                      requesterLandlinePhone: Option[String], requesterMobilePhone: Option[String],
                                      scheduledMailingDate: Date, soScheduledInstallationDate: Date,
                                      soType: String, soLocaleCode: String, soFase: String, soNumber: Long, soValue: java.lang.Double, soStatus: Option[String])

object ElektroMailingOwnershipRow {
  private val config: Config = ConfigFactory.load()

  def apply(row: Map[String, Option[Any]]): ElektroMailingOwnershipRow = {
    val indice: Option[Int] = None

    val customerNumber: Option[Int] = convertOptionDoubleToOptionInt(row("Num_Cliente").asInstanceOf[Option[java.lang.Double]])
    val customerName: String = row("Cliente").getOrElse(config.getString("elektro.withoutInformation.message")).asInstanceOf[String]
    val customerEmail: Option[String] = row("Email_Cadastrado").asInstanceOf[Option[String]]
    val customerCpfCnpj: Option[String] = row("Nr_CPF").asInstanceOf[Option[String]]
    val customerIdNumber: Option[String] = row("Nr_RG").asInstanceOf[Option[String]]
    val customerBirthdate: Option[Date] = row("Data_Nascimento").asInstanceOf[Option[Date]]

    val ucNumber: Int = convertOptionDoubleToOptionInt(row("UC").asInstanceOf[Option[java.lang.Double]]).get
    val ucAddress: String = row("Endereco").getOrElse(config.getString("elektro.withoutInformation.message")).asInstanceOf[String]
    val ucNeighborhood: String = row("Bairro").getOrElse(config.getString("elektro.withoutInformation.message")).asInstanceOf[String]
    val ucCity: String = row("Localidade").getOrElse(config.getString("elektro.withoutInformation.message")).asInstanceOf[String]
    val ucState: String = row("UF").getOrElse(config.getString("elektro.withoutInformation.message")).asInstanceOf[String]
    val ucZipCode: String = row("CEP").getOrElse(config.getString("elektro.withoutInformation.message")).asInstanceOf[String]
    val ucZipCodeNetAvailability: Option[String] = None
    val ucMainClass: Option[String] = row("Des_Classe_Principal").asInstanceOf[Option[String]]

    val landlinePhone: Option[String] = row("Telefone_UC").asInstanceOf[Option[String]]
    val mobilePhone: Option[String] = row("Celular_UC").asInstanceOf[Option[String]]
    val requesterLandlinePhone: Option[String] = row("Tel_Solicitante").asInstanceOf[Option[String]]
    val requesterMobilePhone: Option[String] = row("Cel_Solicitante").asInstanceOf[Option[String]]

    val scheduledMailingDate: Date = row("Data_Emissao").getOrElse(config.getString("elektro.withoutInformation.message")).asInstanceOf[Date]
    val soScheduledInstallationDate: Date = row("Data_Programada").getOrElse(config.getString("elektro.withoutInformation.message")).asInstanceOf[Date]
    val soType: String = row("Des_Sub_Tipo_OS").getOrElse(config.getString("elektro.withoutInformation.message")).asInstanceOf[String]
    val soLocaleCode: String = row("Cod_Local").getOrElse(config.getString("elektro.withoutInformation.message")).asInstanceOf[String]
    val soFase: String = row("Fase").getOrElse("").asInstanceOf[String]
    val soNumber: Long = row("Numero_OS").asInstanceOf[Option[java.lang.Double]].get.toLong
    val soValue: java.lang.Double = row("Valor_Fat").getOrElse(config.getString("elektro.withoutInformation.message")).asInstanceOf[java.lang.Double]
    val soStatus: Option[String] = customerCpfCnpj match {
      case Some(x) => if (x.length > 11) Some("Rejeitada PJ") else None
      case _ => None
    }

    ElektroMailingOwnershipRow.apply(indice, customerNumber, customerName, customerEmail, customerCpfCnpj, customerIdNumber,
      customerBirthdate, ucNumber, ucAddress, ucNeighborhood, ucCity, ucState, ucZipCode, ucZipCodeNetAvailability,
      ucMainClass, landlinePhone, mobilePhone, requesterLandlinePhone, requesterMobilePhone, scheduledMailingDate,
      soScheduledInstallationDate, soType, soLocaleCode, soFase, soNumber, soValue, soStatus)
  }

  private def convertOptionDoubleToOptionInt(x: Option[java.lang.Double]): Option[Int] = {
    if (x.isEmpty) {
      return None
    }

    Some(x.get.toInt)
  }
}
