package br.com.escale.helix.models

case class ElektroMailingOwnership(attachment: Attachment, rows: List[ElektroMailingOwnershipRow])

object ElektroMailingOwnership {
  def apply(attachment: Attachment, rows: => List[Map[String, Option[Any]]]): ElektroMailingOwnership = {
    ElektroMailingOwnership.apply(attachment, rows.map(row => ElektroMailingOwnershipRow(row)))
  }
}
