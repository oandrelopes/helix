package br.com.escale.helix.models

case class ElektroMailing(attachment: Attachment, rows: List[ElektroMailingRow])

object ElektroMailing {
  def apply(attachment: Attachment, rows: => List[Map[String, Option[Any]]]): ElektroMailing = {
    ElektroMailing.apply(attachment, rows.map(row => ElektroMailingRow(row)))
  }
}
