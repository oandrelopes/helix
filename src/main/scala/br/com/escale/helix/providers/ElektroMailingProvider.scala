package br.com.escale.helix.providers

import java.sql.BatchUpdateException
import java.util.Properties

import br.com.escale.helix.NetZipCodesProvider
import br.com.escale.helix.helpers.{UnexpectedHeadersException, XLSXHelper}
import br.com.escale.helix.models.{Attachment, ElektroMailing}
import com.typesafe.config.{Config, ConfigFactory}
import scalikejdbc._
import scalikejdbc.config._

object ElektroMailingProvider {
  DBs.setup('helix)
  private val config: Config = ConfigFactory.load()

  private val upsertQueryTemplate =
    """
        INSERT INTO elektro_mailing AS em (
          uc_number, customer_number, customer_name, customer_email,
          customer_cpf_cnpj, customer_id_number, customer_birthdate, uc_address, uc_neighborhood, uc_city,
          uc_state, uc_zip_code, uc_main_class, landline_phone, mobile_phone,
          requester_landline_phone, requester_mobile_phone, scheduled_mailing_date, so_scheduled_installation_date,
          so_type, so_locale_code, so_number, so_status
        ) VALUES (
          ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
        ) ON CONFLICT ON CONSTRAINT elektro_mailing_pkey DO UPDATE set uc_number=?, customer_number=?, customer_name=?, customer_email=?,
          customer_cpf_cnpj=?, customer_id_number=?, customer_birthdate=?, uc_address=?, uc_neighborhood=?,
          uc_city=?, uc_state=?, uc_zip_code=?, uc_main_class=?, landline_phone=?, mobile_phone=?,
          requester_landline_phone=?, requester_mobile_phone=?, scheduled_mailing_date=?, so_type=?,
          so_locale_code=?, so_status=?
        WHERE em.scheduled_mailing_date < ?
      """

  private def elektroMailingHeaders: List[String] = List("", "UC", "Num_Cliente", "Cliente", "Nr_CPF", "Nr_RG",
    "Data_Nascimento", "Endereco", "Bairro", "Localidade", "UF", "CEP", "Telefone_UC", "Celular_UC",
    "Tel_Solicitante", "Cel_Solicitante", "Email_Cadastrado", "Data_Emissao", "Data_Programada",
    "Des_Sub_Tipo_OS", "Cod_Local", "Des_Classe_Principal", "Numero_OS")
}

class ElektroMailingProvider {

  import ElektroMailingProvider._

  protected lazy val workingDayProvider: WorkingDayProvider = new WorkingDayProvider

  private def props: Properties = {
    val props = new Properties()
    props.put("mail.smtp.host", config.getString("elektro.mail.server.smtp.host"))
    props.put("mail.smtp.socketFactory.port", config.getString("elektro.mail.server.smtp.socketFactory.port"))
    props.put("mail.smtp.socketFactory.class", config.getString("elektro.mail.server.smtp.socketFactory.class"))
    props.put("mail.smtp.auth", config.getString("elektro.mail.server.smtp.auth"))
    props.put("mail.smtp.port", config.getString("elektro.mail.server.smtp.port"))
    props
  }

  private val gmail: GmailProvider = new GmailProvider(props,
    config.getString("elektro.mail.credentials.user"), config.getString("elektro.mail.credentials.password"))

  def getElektroMailing: List[ElektroMailing] = {
    val attachments: List[Attachment] = gmail.getAttachments(config.getString("elektro.mail.server.smtp.readyForProcessingFolder"),
      config.getString("elektro.mail.regex.subject").r, ".xlsx")

    val filesData: List[(Attachment, List[Map[String, Option[Any]]])] = attachments.flatMap { attachment =>
      try {
        val data: List[Map[String, Option[Any]]] =
          XLSXHelper.extractDataFromFileWithExpectedHeaders(attachment.file, elektroMailingHeaders)

        Some((attachment, data))
      } catch {
        case invalidHeaders: UnexpectedHeadersException =>
          printf(s"File ${attachment.file.getName} is a invalid Elektro mailing XLSX: ${invalidHeaders.getMessage}\n")
          None
        case e: Exception => throw e
      }
    }

    filesData.map(x => ElektroMailing(x._1, x._2))
  }

  @throws[BatchUpdateException]
  def store(mailing: ElektroMailing): Unit = {

    val args: Seq[Seq[Any]] = mailing.rows.map { row =>
      Seq[Any](row.ucNumber, row.customerNumber, row.customerName, row.customerEmail, row.customerCpfCnpj, row.customerIdNumber,
        row.customerBirthdate, row.ucAddress, row.ucNeighborhood, row.ucCity, row.ucState, row.ucZipCode,
        row.ucMainClass, row.landlinePhone, row.mobilePhone, row.requesterLandlinePhone, row.requesterMobilePhone,
        row.scheduledMailingDate, row.soScheduledInstallationDate, row.soType, row.soLocaleCode, row.soNumber,
        row.soStatus, row.ucNumber, row.customerNumber, row.customerName, row.customerEmail, row.customerCpfCnpj,
        row.customerIdNumber, row.customerBirthdate, row.ucAddress, row.ucNeighborhood, row.ucCity,
        row.ucState, row.ucZipCode, row.ucMainClass, row.landlinePhone, row.mobilePhone, row.requesterLandlinePhone,
        row.requesterMobilePhone, row.scheduledMailingDate, row.soType, row.soLocaleCode, row.soStatus,
        row.scheduledMailingDate)
    }

    val sql = SQL(upsertQueryTemplate)
    try {
      NamedDB('helix) localTx(implicit session => {
        sql.batch(args: _*).apply()
      })
    } catch {
      case e: BatchUpdateException =>
        printf("\n\n %s \n\n", e.getNextException.getMessage)
        throw e
    }
  }

  def getOpenServicesOrders: List[Map[String, Any]] = {
    updateZipCodesNetAvailability
    groupServicesOrdersWithSameAddressAndCustomer(getOpenServicesOrdersWithScheduledInstallationDateToNextWorkingDay)
  }

  def markDone(mailing: ElektroMailing): Unit = {
    val subject: String = mailing.attachment.email.getSubject
    val from: String = config.getString("elektro.mail.server.smtp.readyForProcessingFolder")
    val to: String = config.getString("elektro.mail.server.smtp.processingDoneFolder")

    gmail.moveEmailWithSubjectToFolder(subject, from, to)
  }

  def getDBHeaders: List[String] = {
    List("indice", "uc_number", "uc_zip_code_net_availability",
      "customer_number", "customer_name", "customer_cpf_cnpj", "customer_id_number", "customer_birthdate", "uc_address",
      "uc_neighborhood", "uc_city", "uc_state", "uc_zip_code", "landline_phone", "mobile_phone", "requester_mobile_phone",
      "requester_landline_phone", "customer_email", "scheduled_mailing_date", "so_scheduled_installation_date",
      "so_type", "so_number", "so_status")
  }

  def getHeadersMapFromDBToCSV: Map[String, String] = {
    Map(
      "indice" -> "Indice",
      "uc_number" -> "UC",
      "uc_zip_code_net_availability" -> "CEPCabeadoNET",
      "customer_number" -> "NumeroCliente",
      "customer_name" -> "NomeCliente",
      "customer_cpf_cnpj" -> "CPF",
      "customer_id_number" -> "RG",
      "customer_birthdate" -> "DataNascimentoCliente",
      "uc_address" -> "EnderecoCliente",
      "uc_neighborhood" -> "Bairro",
      "uc_city" -> "Localidade",
      "uc_state" -> "UF",
      "uc_zip_code" -> "CEP",
      "landline_phone" -> "TelUC",
      "mobile_phone" -> "CelUC",
      "requester_mobile_phone" -> "CelSolicitante",
      "requester_landline_phone" -> "TelSolicitante",
      "customer_email" -> "EmailCliente",
      "scheduled_mailing_date" -> "DataMailing",
      "so_scheduled_installation_date" -> "DataAgendada",
      "so_type" -> "TipoServico",
      "so_number" -> "OrdemServico",
      "so_status" -> "SoStatus"
    )
  }

  private def updateZipCodesNetAvailability() = {
    val zipCodes: List[String] = ElektroDbProvider.retrieveOpenServiceOrdersZipCodesWithScheduledInstallationDate(workingDayProvider.next)
    val wiredZipCodes: List[String] = NetZipCodesProvider.get(zipCodes)
    val nonWiredZipCodes: List[String] = zipCodes.diff(wiredZipCodes)
    if(wiredZipCodes.nonEmpty) {
      ElektroDbProvider.updateZipCodesWithWiredNetAvailability(wiredZipCodes)
    }
    if(nonWiredZipCodes.nonEmpty) {
      ElektroDbProvider.updateZipCodesWithNonWiredNetAvailability(nonWiredZipCodes)
    }
  }

  private def getOpenServicesOrdersWithScheduledInstallationDateToNextWorkingDay: List[Map[String, Any]] = {
    ElektroDbProvider.retrieveOpenServicesOrdersWithScheduledInstallationDate(workingDayProvider.next)
  }

  private def groupServicesOrdersWithSameAddressAndCustomer(mailing: List[Map[String, Any]]): List[Map[String, Any]] = {
    mailing.groupBy(row => (
      row("customer_number"),
      row("uc_zip_code"),
      row("uc_neighborhood"),
      row("so_scheduled_installation_date")
    )).values.map(_.reduceLeft { (result, next) =>
      val address = result.getOrElse("uc_address", "") + "&#13;&#10;" + next("uc_address").toString
      result.updated("uc_address", address)
    }).toList
  }

}
