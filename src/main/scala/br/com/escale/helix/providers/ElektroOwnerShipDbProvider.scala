package br.com.escale.helix.providers

import java.time.LocalDate

import scalikejdbc._
import scalikejdbc.config._

object ElektroOwnerShipDbProvider {
  DBs.setup('helix)

  def retrieveOpenServiceOrdersZipCodesWithScheduledInstallationDate(date: LocalDate): List[String] = {
    NamedDB('helix) readOnly (implicit session => {
      sql"""
          SELECT uc_zip_code FROM elektro_mailing_ownership AS elek
          WHERE
            elek.so_scheduled_installation_date = $date""".map {
        _.string("uc_zip_code")
      }.list.apply()
    })
  }

  def updateZipCodesWithWiredNetAvailability(wiredZipCodes: List[String]) = {
    NamedDB('helix) localTx(implicit session => {
      sql"""
        UPDATE elektro_mailing_ownership SET uc_zip_code_net_availability = 'CABEADO'
          WHERE uc_zip_code IN ($wiredZipCodes)
      """.update.apply()
    })
  }

  def updateZipCodesWithNonWiredNetAvailability(nonWiredZipCodes: List[String]) = {
    NamedDB('helix) localTx(implicit session => {
      sql"""
        UPDATE elektro_mailing_ownership SET uc_zip_code_net_availability = 'NAO CABEADO'
          WHERE uc_zip_code IN ($nonWiredZipCodes)
      """.update.apply()
    })
  }

  def retrieveOpenServicesOrdersWithScheduledInstallationDate(scheduledInstallationDate: LocalDate): List[Map[String, Any]] = {
    NamedDB('helix) readOnly (implicit session => {
        sql"""
        SELECT
         id,
         uc_number,
         uc_zip_code_net_availability,
         customer_number,
         customer_name,
         customer_cpf_cnpj,
         customer_id_number,
         customer_birthdate,
         uc_address,
         uc_neighborhood,
         uc_city,
         uc_state,
         uc_zip_code,
         landline_phone,
         mobile_phone,
         requester_mobile_phone,
         requester_landline_phone,
         customer_email,
         scheduled_mailing_date,
         so_scheduled_installation_date,
         so_type,
         so_number,
         so_status
         FROM elektro_mailing_ownership AS elek
          WHERE
            elek.so_scheduled_installation_date = $scheduledInstallationDate
        """.map {
        _.toMap
      }.list.apply()
    })
  }
}
