package br.com.escale.helix

import scalikejdbc._
import scalikejdbc.config.DBs

package object NetZipCodesProvider {
  def get(zipCodes: List[String]): List[String] = {
    if (zipCodes.isEmpty) {
      print("There are no zip codes to retrieve wired status")
      return List[String]()
    }

    DBs.setup('netpromo)

    NamedDB('netpromo) readOnly { implicit session => {
      sql"""SELECT cep FROM cep_status WHERE cep IN ($zipCodes)""".map {
        _.string("cep")
      }.list.apply()
    }}
  }
}
