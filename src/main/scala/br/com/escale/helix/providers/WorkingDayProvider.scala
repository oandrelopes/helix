package br.com.escale.helix.providers

import java.time.{LocalDate, Year}

class WorkingDayProvider {

  protected lazy val holidaysProvider: HolidaysProvider = new HolidaysProvider
  private val currentYear: Int = Year.now().getValue
  private val holidays: List[String] = holidaysProvider.getHolidays(Option(currentYear), None, None)

  def next: LocalDate = {
    nextWithDate(getCurrentDate)
  }

  def todayIsWorkingDay: Boolean = {
    isWorkingDay(getCurrentDate)
  }

  def isWorkingDay(date: LocalDate): Boolean = {
    holidays.indexOf(date.toString) < 0 && date.getDayOfWeek.getValue <= 5
  }

  protected def getCurrentDate: LocalDate = LocalDate.now

  private def nextWithDate(date: LocalDate): LocalDate = {
    val nextDay = date.plusDays(1)
    if (isWorkingDay(nextDay)) {
      nextDay
    } else {
      nextWithDate(nextDay)
    }
  }
}

