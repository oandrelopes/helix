package br.com.escale.helix.helpers

import java.io.File

import com.github.tototoshi.csv._

object CSVHelper {

  @throws[UnexpectedEmptyDataException]
  def writeFile(fileName: String, data: List[List[Any]]): Unit = {
    if (data.isEmpty) {
      throw UnexpectedEmptyDataException(s"Cannot write CSV file with no data")
    }

    val csv = CSVWriter.open(new File("/tmp/" + fileName))
    csv.writeAll(data)
    csv.close()
  }
}
