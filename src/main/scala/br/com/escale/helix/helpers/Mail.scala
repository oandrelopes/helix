package br.com.escale.helix.helpers

import java.text.SimpleDateFormat
import java.util.{Date, Properties}
import javax.activation.{DataHandler, DataSource, FileDataSource}
import javax.mail._
import javax.mail.internet._

import com.typesafe.config.{Config, ConfigFactory}

case class Mail() {

  private val config: Config = ConfigFactory.load()
  private val formatter = new SimpleDateFormat("dd-MM-yyyy")

  private def addAttachmentAndBody(content: String, attachment:String, message: MimeMessage): MimeMessage = {
    val messageBodyPart: MimeBodyPart = new MimeBodyPart()
    messageBodyPart.setText(content)

    val attachmentBody: MimeBodyPart = new MimeBodyPart()
    val source: DataSource = new FileDataSource(attachment)
    attachmentBody.setDataHandler(new DataHandler(source))
    attachmentBody.setFileName(source.getName)

    val multipart: Multipart = new MimeMultipart()
    multipart.addBodyPart(messageBodyPart)
    multipart.addBodyPart(attachmentBody)
    message.setContent(multipart)
    message
  }

  private def sendEmail(from: String, to: String, cc: String, subject: String, content: String, attachment: String) {
    val message: MimeMessage = generateMailMessage
    message.setFrom(new InternetAddress(from))
    message.setSentDate(new Date())
    message.setSubject(subject)
    message.setRecipient(Message.RecipientType.TO, new InternetAddress(to))

    val messageWithAttachment = addAttachmentAndBody(content, attachment, message)

    if (messageWithAttachment != null ) {
      Transport.send(messageWithAttachment)
    }
  }

  private def buildAuthSession(properties: Properties, authenticator: Authenticator): Session = {
    Session.getInstance(properties, authenticator)
  }

  def generateElektroMailProps(): Properties = {
    val properties = new Properties()
    properties.put("mail.smtp.host", config.getString("elektro.mail.server.smtp.host"))
    properties.put("mail.smtp.socketFactory.port", config.getString("elektro.mail.server.smtp.socketFactory.port"))
    properties.put("mail.smtp.socketFactory.class", config.getString("elektro.mail.server.smtp.socketFactory.class"))
    properties.put("mail.smtp.auth", config.getString("elektro.mail.server.smtp.auth"))
    properties.put("mail.smtp.port", config.getString("elektro.mail.server.smtp.port"))
    properties
  }

  def generateElektroMailAuthenticator(): Authenticator = {
    generateMailAuthenticator(config.getString("elektro.mail.credentials.user"),
      config.getString("elektro.mail.credentials.password"))
  }

  def generateMailAuthenticator(user: String, password: String): Authenticator = {
    new Authenticator() {
      override def getPasswordAuthentication = new PasswordAuthentication(user, password)
    }
  }

  def generateMailMessage: MimeMessage = {
    new MimeMessage(buildAuthSession(generateElektroMailProps(), generateElektroMailAuthenticator()))
  }

  def sendElektroMail(attachmentPath: String): Unit = {
    sendEmail(
      config.getString("elektro.mail.message.from"),
      config.getString("elektro.mail.message.to"),
      config.getString("elektro.mail.message.cc"),
      config.getString("elektro.mail.message.subject") + formatter.format(new Date()),
      config.getString("elektro.mail.message.content"),
      attachmentPath
    )
  }
}
