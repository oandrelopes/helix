package br.com.escale.helix.helpers

case class UnexpectedEmptyDataException (private val message: String = "", private val cause: Throwable = None.orNull)
  extends Exception(message, cause)
