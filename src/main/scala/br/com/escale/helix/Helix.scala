package br.com.escale.helix

import java.nio.file.{Files, Paths}
import java.text.SimpleDateFormat
import java.util.Date
import java.time.LocalDate

import br.com.escale.helix.helpers.{CSVHelper, Mail}
import br.com.escale.helix.models.{ElektroMailing, ElektroMailingOwnership}
import br.com.escale.helix.providers.{ElektroMailingOwnershipProvider, ElektroMailingProvider, WorkingDayProvider}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Helix extends App {
  var workingDay: WorkingDayProvider = if (workingDay == null) new WorkingDayProvider() else workingDay
  private lazy val provider = new ElektroMailingProvider()
  private lazy val ownershipProvider = new ElektroMailingOwnershipProvider()

  if (args.length != 1) {
    printHelp()
    System.exit(0)
  }

  args(0) match {
    case "import-elektro-mailing" => importElektroMailing()
    case "import-elektro-ownership" => importElektroMailingOwnership()
    case "send-elektro-mailing" => sendElektroMailing()
    case "send-elektro-ownership" => sendElektroMailingOwnerShip()
    case _ => printHelp()
  }

  private def importElektroMailing(): Unit = {
    val mailings: List[ElektroMailing] = provider.getElektroMailing

    mailings.foreach { mailing =>
      provider.store(mailing)
      provider.markDone(mailing)
    }
  }

  private def importElektroMailingOwnership(): Unit = {
    val mailings: List[ElektroMailingOwnership] = ownershipProvider.getElektroOwnershipMailing

    mailings.foreach { mailing =>
      ownershipProvider.storeOwnership(mailing)
      ownershipProvider.markDoneOwnership(mailing)
    }
  }

  private def sendElektroMailingOwnerShip(): Unit = {
    isWorkingDay()

    val fileName = createCsvTitle("Elektro-Mailing-OwnerShip_")
    val filePath = "/tmp/" + fileName

    val dataAsMap: List[Map[String, Any]] = ownershipProvider.getOpenServicesOrders
    val dbHeaders: List[String] = ownershipProvider.getDBHeaders
    val dbHeadersToCSVHeaders: Map[String, String] = ownershipProvider.getHeadersMapFromDBToCSV

    val data: mutable.MutableList[List[Any]] = mutable.MutableList(dbHeaders.map(dbHeadersToCSVHeaders(_)))
    data ++= dataAsMap.map { row =>
      val rowBuffer: ArrayBuffer[Any] = ArrayBuffer()
      for (i <- dbHeaders.indices) {
        rowBuffer += row.getOrElse(dbHeaders(i), "")
      }
      rowBuffer.toList
    }

    sendEmail(fileName, data, filePath)
  }

  private def sendElektroMailing(): Unit = {
    isWorkingDay()

    val fileName = createCsvTitle("Elektro-Mailing_")
    val filePath = "/tmp/" + fileName

    val dataAsMap: List[Map[String, Any]] = provider.getOpenServicesOrders
    val dbHeaders: List[String] = provider.getDBHeaders
    val dbHeadersToCSVHeaders: Map[String, String] = provider.getHeadersMapFromDBToCSV

    val data: mutable.MutableList[List[Any]] = mutable.MutableList(dbHeaders.map(dbHeadersToCSVHeaders(_)))
    data ++= dataAsMap.map { row =>
      val rowBuffer: ArrayBuffer[Any] = ArrayBuffer()
      for (i <- dbHeaders.indices) {
        rowBuffer += row.getOrElse(dbHeaders(i), "")
      }
      rowBuffer.toList
    }

    sendEmail(fileName, data, filePath)
  }

  private def createCsvTitle(title: String): String = {
    val formatter = new SimpleDateFormat("dd-MM-yyyy")
    title + formatter.format(new Date()) + ".csv"
  }

  private def sendEmail(fileName: String, csvData: mutable.MutableList[List[Any]], filePath: String): Unit ={
    CSVHelper.writeFile(fileName, csvData.toList)
    Mail().sendElektroMail(filePath)
    Files.delete(Paths.get(filePath))
  }

  private def isWorkingDay(): Unit = {
    if(!workingDay.todayIsWorkingDay) {
      printf(s"[${LocalDate.now}] Today is not a workday. Is not necessary to send Elektro Mailing.")
      System.exit(0)
    }
  }

  private def printHelp(): Unit = {
    print(
      """
Helix is Escale's mailing generator

Usage:
    helix [command]

Available commands are:

[import]
  - import-elektro-mailing
  - import-elektro-ownership

[send]
  - send-elektro-mailing
  - send-elektro-ownership

Escale 2017 - www.escale.com.br

""")
  }

  def setWorkingDayProvider(workingDayProvider: WorkingDayProvider) {
    workingDay = workingDayProvider
  }
}
