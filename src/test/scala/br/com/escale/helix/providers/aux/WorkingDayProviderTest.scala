package br.com.escale.helix.providers.aux

import java.time.LocalDate

import br.com.escale.helix.providers.WorkingDayProvider

class WorkingDayProviderTest extends WorkingDayProvider {
  protected var mockedDate: LocalDate = LocalDate.now

  override protected lazy val holidaysProvider: HolidaysProviderTest = new HolidaysProviderTest

  override protected def getCurrentDate: LocalDate = mockedDate

  def setMockedDate(date: String): Unit = {
    mockedDate = LocalDate.parse(date)
  }
}
