package br.com.escale.helix.providers.spec

import br.com.escale.helix.providers.aux.HolidaysProviderTest
import org.scalatest.{Matchers, WordSpecLike}

class HolidaysProviderSpec extends WordSpecLike with Matchers {
  val holidaysProviderTest: HolidaysProviderTest = new HolidaysProviderTest
  "HolidaysProvider" must {
    "get holidays" in {
      val expected: List[String] = List("2017-05-11", "2017-11-20")
      val holidays = holidaysProviderTest.getHolidays(None, None, None)
      holidays should be (expected)
    }
  }
}
