package br.com.escale.helix.providers.aux

import br.com.escale.helix.providers.HolidaysProvider

class HolidaysProviderTest extends HolidaysProvider {
  override protected def retriveHolidaysOnCalendarioApi(year: Option[Int], state: Option[String], city: Option[String]): String = {
    """<?xml version='1.0' encoding='UTF-8'?><events>
        <location>
          <city>SAO PAULO</city>
          <state>SP</state>
          <country>Brasil</country>
        </location>
        <event>
          <date>11/05/2017</date>
          <name>Aniversário do Rafa</name>
          <description>Feriado na quinta-feira</description>
          <type>Feriado Nacional</type>
          <type_code>1</type_code>
        </event>
        <event>
          <date>28/02/2017</date>
          <name>Carnaval</name>
          <description>Carnaval NÃO é um feriado oficial, é Ponto Facultativo, ou seja, cabe às empresas e orgão públicos decidirem se trabalharão ou não.</description>
          <type>Facultativo</type>
          <type_code>4</type_code>
        </event>
        <event>
          <date>01/04/2017</date>
          <name>Dia da Mentira</name>
          <description>Dia da mentira, dia das petas, dia dos tolos ou dia dos bobos.</description>
          <type>Dia Convencional</type>
          <type_code>9</type_code>
        </event>
        <event>
          <date>20/11/2017</date>
          <name>Consciência negra</name>
          <description>Feriado na segunda-feira</description>
          <type>Feriado Municipal</type>
          <type_code>1</type_code>
        </event>
      </events>"""
  }
}
