package br.com.escale.helix.providers.spec

import java.time.LocalDate

import br.com.escale.helix.providers.aux.WorkingDayProviderTest
import org.scalatest.{Matchers, WordSpecLike}

class WorkingDayProviderSpec extends WordSpecLike with Matchers {

  "WorkingDayProviderSpec" must {
    val wd = new WorkingDayProviderTest()
    "get next working day" in {
      wd.setMockedDate("2017-09-27")
      val nextWorkingDay: LocalDate = wd.next
      val expected: LocalDate = LocalDate.parse("2017-09-28")
      nextWorkingDay should be (expected)
    }

    "get working day after a weekend" in {
      wd.setMockedDate("2017-09-29")
      val nextWorkingDay: LocalDate = wd.next
      val expected: LocalDate = LocalDate.parse("2017-10-02")
      nextWorkingDay should be (expected)
    }

    "get working day after a holiday" in {
      wd.setMockedDate("2017-05-10")
      val nextWorkingDay: LocalDate = wd.next
      val expected: LocalDate = LocalDate.parse("2017-05-12")
      nextWorkingDay should be (expected)
    }

    "get working day after weekend and monday holiday " in {
      wd.setMockedDate("2017-11-17")
      val nextWorkingDay: LocalDate = wd.next
      val expected: LocalDate = LocalDate.parse("2017-11-21")
      nextWorkingDay should be (expected)
    }

    "verify if today is a workday on holiday" in {
      wd.setMockedDate("2017-11-20")
      val todayIsWorkingDay: Boolean = wd.todayIsWorkingDay
      val expected: Boolean = false
      todayIsWorkingDay should be (expected)
    }

    "verify if today is a workday on weekend" in {
      wd.setMockedDate("2017-10-21")
      val todayIsWorkingDay: Boolean = wd.todayIsWorkingDay
      val expected: Boolean = false
      todayIsWorkingDay should be (expected)
    }

    "verify if today is a workday on a monday without holiday" in {
      wd.setMockedDate("2017-10-23")
      val todayIsWorkingDay: Boolean = wd.todayIsWorkingDay
      val expected: Boolean = true
      todayIsWorkingDay should be (expected)
    }
  }
}
