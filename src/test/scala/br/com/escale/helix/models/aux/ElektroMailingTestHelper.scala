package br.com.escale.helix.models.aux

import java.util.Date

import br.com.escale.helix.models.ElektroMailingRow

object ElektroMailingTestHelper {

  def compareRowWithMap(row: ElektroMailingRow, map: Map[String, Option[Any]]): Boolean = {
    row.customerNumber.get == map("Num_Cliente").get.asInstanceOf[java.lang.Double].toInt &&
    row.customerName == map("Cliente").get.asInstanceOf[String] &&
    row.customerEmail.get == map("Email_Cadastrado").get.asInstanceOf[String] &&
    row.customerCpfCnpj.get == map("Nr_CPF").get.asInstanceOf[String] &&
    row.customerIdNumber.get == map("Nr_RG").get.asInstanceOf[String] &&
    row.customerBirthdate.get == map("Data_Nascimento").get.asInstanceOf[Date] &&
    row.ucNumber == map("UC").get.asInstanceOf[java.lang.Double].toInt &&
    row.ucAddress == map("Endereco").get.asInstanceOf[String] &&
    row.ucNeighborhood == map("Bairro").get.asInstanceOf[String] &&
    row.ucCity == map("Localidade").get.asInstanceOf[String] &&
    row.ucState == map("UF").get.asInstanceOf[String] &&
    row.ucZipCode == map("CEP").get.asInstanceOf[String] &&
    row.ucZipCodeNetAvailability.isEmpty &&
    row.ucMainClass.get == map("Des_Classe_Principal").get.asInstanceOf[String] &&
    row.landlinePhone.get == map("Telefone_UC").get.asInstanceOf[String] &&
    row.mobilePhone.get == map("Celular_UC").get.asInstanceOf[String] &&
    row.requesterLandlinePhone.get == map("Tel_Solicitante").get.asInstanceOf[String] &&
    row.requesterMobilePhone.get == map("Cel_Solicitante").get.asInstanceOf[String] &&
    row.scheduledMailingDate == map("Data_Emissao").get.asInstanceOf[Date] &&
    row.soScheduledInstallationDate == map("Data_Programada").get.asInstanceOf[Date] &&
    row.soType == map("Des_Sub_Tipo_OS").get.asInstanceOf[String] &&
    row.soLocaleCode == map("Cod_Local").get.asInstanceOf[String] &&
    row.soNumber == map("Numero_OS").get.asInstanceOf[java.lang.Double].toLong &&
    row.soStatus.isEmpty
  }

}
