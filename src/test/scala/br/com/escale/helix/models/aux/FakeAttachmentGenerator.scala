package br.com.escale.helix.models.aux

import java.io.File
import javax.mail.Session
import javax.mail.internet.MimeMessage

import br.com.escale.helix.models.Attachment

object FakeAttachmentGenerator {

  def generate(): Attachment = {
    Attachment(new MimeMessage(null.asInstanceOf[Session]), new File("/tmp/fake.attachment"))
  }

}
