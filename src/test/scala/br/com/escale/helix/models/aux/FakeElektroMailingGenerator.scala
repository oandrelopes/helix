package br.com.escale.helix.models.aux

import br.com.escale.helix.models.{ElektroMailing, ElektroMailingRow}
import br.com.escale.helix.testhelpers.Faker

import scala.collection.mutable

object FakeElektroMailingGenerator {

  def generateFakeElektroMailing(): ElektroMailing = {
    val n = Faker.intBetween(1, 20)

    val mutableList: mutable.MutableList[ElektroMailingRow] = mutable.MutableList()
    for (i <- 0 until n) {
      mutableList += generateFakeElektroMailingRow()
    }

    ElektroMailing.apply(FakeAttachmentGenerator.generate(), mutableList.toList)
  }

  def generateFakeElektroMailingRow(): ElektroMailingRow = {
    ElektroMailingRow.apply(Option(Faker.int), Option(Faker.int), Faker.name, Option(Faker.email), Option(Faker.cpf),
      Option(Faker.string), Option(Faker.date), Faker.int, Faker.address, Faker.string, Faker.city, Faker.state, Faker.zipCode,
      Option(Faker.string), Option(Faker.string), Option(Faker.phoneNumber), Option(Faker.phoneNumber), Option(Faker.phoneNumber),
      Option(Faker.phoneNumber), Faker.date, Faker.date, Faker.string, Faker.string, Faker.long, Option(Faker.string))
  }

  def generateFakeElektroMailingRowWithoutOptionals(): ElektroMailingRow = {
    ElektroMailingRow.apply(None, None, Faker.name, None, None, None, None, Faker.int, Faker.address, Faker.string,
      Faker.city, Faker.state, Faker.zipCode, None, None, None, None, None, None, Faker.date, Faker.date, Faker.string,
      Faker.string, Faker.long, None)
  }

  def generateFakeElektroMailingRowAsMap(): Map[String, Option[Any]] = {
    Map(
      "" -> Option("SHOULDN'T MATTER"), "UC" -> Option(Faker.int.toDouble), "Num_Cliente" -> Option(Faker.int.toDouble),
      "Cliente" -> Option(Faker.name), "Nr_CPF" -> Option(Faker.cpf), "Nr_RG" -> Option(Faker.int.toString),
      "Data_Nascimento" -> Option(Faker.date), "Endereco" -> Option(Faker.address), "Bairro" -> Option(Faker.string),
      "Localidade" -> Option(Faker.city), "UF" -> Option(Faker.state), "CEP" -> Option(Faker.zipCode),
      "Telefone_UC" -> Option(Faker.phoneNumber), "Celular_UC" -> Option(Faker.phoneNumber),
      "Tel_Solicitante" -> Option(Faker.phoneNumber), "Cel_Solicitante" -> Option(Faker.phoneNumber),
      "Email_Cadastrado" -> Option(Faker.email), "Data_Emissao" -> Option(Faker.date),
      "Data_Programada" -> Option(Faker.date), "Des_Sub_Tipo_OS" -> Option(Faker.string),
      "Cod_Local" -> Option(Faker.string), "Des_Classe_Principal" -> Option(Faker.string), "Numero_OS" -> Option(Faker.int.toDouble)
    )
  }

}
