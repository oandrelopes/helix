package br.com.escale.helix.models.spec

import br.com.escale.helix.models.ElektroMailing
import br.com.escale.helix.models.aux.{ElektroMailingTestHelper, FakeAttachmentGenerator, FakeElektroMailingGenerator}
import br.com.escale.helix.testhelpers.Faker
import org.scalatest.{Matchers, WordSpecLike}

import scala.collection.mutable

class ElektroMailingSpec extends WordSpecLike with Matchers {

  "ElektroMailing" must {

    "be correctly instantiated when given a list of ElektroMailingRow's" in {
      val expected: ElektroMailing = FakeElektroMailingGenerator.generateFakeElektroMailing()

      val actual = ElektroMailing.apply(expected.attachment, expected.rows)

      actual should be (expected)
    }

    "be correctly instantiated when given a List[Map[String, Option[Any]]]" in {
      val attachment = FakeAttachmentGenerator.generate()
      val input: mutable.MutableList[Map[String, Option[Any]]] = mutable.MutableList()

      val length: Int = Faker.intBetween(1, 20)
      for (i <- 0 until length) {
        input += FakeElektroMailingGenerator.generateFakeElektroMailingRowAsMap()
      }

      val mailing = ElektroMailing.apply(attachment, input.toList)

      for (i <- 0 until length) {
        ElektroMailingTestHelper.compareRowWithMap(mailing.rows(i), input(i)) should be (true)
      }
    }

  }

}
