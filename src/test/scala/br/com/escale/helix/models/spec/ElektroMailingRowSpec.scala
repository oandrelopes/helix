package br.com.escale.helix.models.spec

import br.com.escale.helix.models.ElektroMailingRow
import br.com.escale.helix.models.aux.{ElektroMailingTestHelper, FakeElektroMailingGenerator}
import org.scalatest.{Matchers, WordSpecLike}

class ElektroMailingRowSpec extends WordSpecLike with Matchers {

  "ElektroMailingRow" must {

    "be correctly instantiated from a Map[String, Option[Any]]" in {
      val sampleMailingRowAsMap = FakeElektroMailingGenerator.generateFakeElektroMailingRowAsMap()
      val row = ElektroMailingRow.apply(sampleMailingRowAsMap)

      ElektroMailingTestHelper.compareRowWithMap(row, sampleMailingRowAsMap) should be (true)
    }

  }

}
