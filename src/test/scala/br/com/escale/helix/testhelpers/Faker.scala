package br.com.escale.helix.testhelpers

import java.time.{LocalDate, ZoneOffset}
import java.util.Date

import scala.util.Random

object Faker {
  private val faker = new com.github.javafaker.Faker()
  private val rand = Random

  def int: Int = {
    long.toInt
  }

  def intBetween(min: Int, max: Int): Int = {
    faker.number().numberBetween(min, max)
  }

  def string: String = {
    faker.lorem().characters(50)
  }

  def long: Long = {
    faker.number().randomNumber()
  }

  def name: String = {
    faker.name().fullName()
  }

  def email: String = {
    faker.internet().emailAddress()
  }

  def date: Date = {
    faker.date().between(Date.from(LocalDate.of(1991, 1, 19).atStartOfDay().toInstant(ZoneOffset.UTC)),
      Date.from(LocalDate.of(2019, 12, 31).atStartOfDay().toInstant(ZoneOffset.UTC)))
  }

  def address: String = {
    faker.address().streetAddress()
  }

  def city: String = {
    faker.address().city()
  }

  def state: String = {
    faker.address().state()
  }

  def zipCode: String = {
    faker.address().zipCode()
  }

  def phoneNumber: String = {
    if (rand.nextInt(9) % 2 == 0) {
      faker.phoneNumber().phoneNumber()
    } else {
      faker.phoneNumber().cellPhone()
    }
  }

  def cpf: String = {
    val maxDigit = 9

    val n1 = rand.nextInt(maxDigit)
    val n2 = rand.nextInt(maxDigit)
    val n3 = rand.nextInt(maxDigit)
    val n4 = rand.nextInt(maxDigit)
    val n5 = rand.nextInt(maxDigit)
    val n6 = rand.nextInt(maxDigit)
    val n7 = rand.nextInt(maxDigit)
    val n8 = rand.nextInt(maxDigit)
    val n9 = rand.nextInt(maxDigit)

    var d1 = n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10
    d1 = 11 - (d1 % 11)
    if (d1 >= 10) {
      d1 = 0
    }

    var d2 = d1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11
    d2 = 11 - (d2 % 11)
    if (d2 >= 10) {
      d2 = 0
    }

    "%d%d%d%d%d%d%d%d%d%d%d".format(n1, n2, n3, n4, n5, n6, n7, n8, n9, d1, d2)
  }
}
