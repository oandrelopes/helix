package br.com.escale.helix

import br.com.escale.helix.providers.aux.WorkingDayProviderTest
import de.saly.javamail.mock2.{MailboxFolder, MockMailbox}
import org.scalatest.{Matchers, WordSpecLike}

class HelixSpec extends WordSpecLike with Matchers {

  "Helix" should {
    "send email only on business day" in {
      val wd = new WorkingDayProviderTest()
      wd.setMockedDate("2017-11-03")
      Helix.setWorkingDayProvider(wd)

      val mb = MockMailbox.get("rafael.pazini@escale.com.br")
      val mf = mb.getInbox

      Helix.main(Array("send-elektro-mailing"))

      mf.getMessageCount should be (1)
      mf.getByMsgNum(1).getSubject should include ("Elektro - Mailing")
    }

    "does not send email on a holiday" in {
      val wd = new WorkingDayProviderTest()
      wd.setMockedDate("2017-11-20")
      Helix.setWorkingDayProvider(wd)

      val mb = MockMailbox.get("rafael.pazini@escale.com.br")
      val mf = mb.getInbox

      Helix.main(Array("send-elektro-mailing"))
      mf.getMessageCount should be (0)
    }

  }

}
