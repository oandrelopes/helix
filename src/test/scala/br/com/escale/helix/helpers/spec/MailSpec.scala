package br.com.escale.helix.helpers.spec

import javax.mail.Authenticator
import javax.mail.internet.MimeMessage

import br.com.escale.helix.helpers.Mail
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.{Matchers, WordSpecLike}

class MailSpec extends WordSpecLike with Matchers {
  val config: Config = ConfigFactory.load()

  "Mail helper" must {

    "check elektro mail properties (host, port, auth)" in {
      val props = Mail().generateElektroMailProps()

      props.getProperty("mail.smtp.host") should be(config.getString("elektro.mail.server.smtp.host"))
      props.getProperty("mail.smtp.socketFactory.port") should be(config.getString("elektro.mail.server.smtp.socketFactory.port"))
      props.getProperty("mail.smtp.socketFactory.class") should be(config.getString("elektro.mail.server.smtp.socketFactory.class"))
      props.getProperty("mail.smtp.auth") should be(config.getString("elektro.mail.server.smtp.auth"))
      props.getProperty("mail.smtp.port") should be(config.getString("elektro.mail.server.smtp.port"))
    }

    "generate an authenticator for email" in {
      val auth = Mail().generateElektroMailAuthenticator()
      auth.isInstanceOf[Authenticator] should be (true)
    }

    "generate elektro mail message" in {
      val message = Mail().generateMailMessage
      message.isInstanceOf[MimeMessage] should be (true)
    }
  }
}
