package br.com.escale.helix.helpers.spec

import java.io.File
import java.util.Date

import br.com.escale.helix.helpers.{UnexpectedHeadersException, XLSXHelper}
import br.com.escale.helix.testhelpers.Faker
import org.apache.poi.ss.usermodel._
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.scalatest.{Matchers, WordSpecLike}

import scala.collection.JavaConverters._

object XLSXHelperSpec {
  val validMailingPath: String = getClass.getResource("/ElektroValidMailing.xlsx").getFile
  val invalidMailingPath: String = getClass.getResource("/ElektroInvalidMailing.xlsx").getFile

  def elektroMailingHeaders: List[String] = List("", "UC", "Num_Cliente", "Cliente", "Nr_CPF", "Nr_RG",
    "Data_Nascimento", "Endereco", "Bairro", "Localidade", "UF", "CEP", "Telefone_UC", "Celular_UC",
    "Tel_Solicitante", "Cel_Solicitante", "Email_Cadastrado", "Data_Emissao", "Data_Programada",
    "Des_Sub_Tipo_OS", "Cod_Local", "Des_Classe_Principal", "Numero_OS")
}

class XLSXHelperSpec extends WordSpecLike with Matchers {

  import XLSXHelperSpec._

  private val sampleWB: Workbook = new XSSFWorkbook()
  private val sampleSheet: Sheet = sampleWB.createSheet("S")
  private val sampleRow: Row = sampleSheet.createRow(0)

  "XLSX helper" must {

    "be able to extract data from a file with expected headers" in {
      val result = XLSXHelper.extractDataFromFileWithExpectedHeaders(new File(validMailingPath), elektroMailingHeaders)
      result.length should be (418)
    }

    "not throw an error when validating valid headers" in {
      val wb: Workbook = WorkbookFactory.create(new File(validMailingPath))
      val sheet = wb.getSheet("SQL Results")

      val headers: List[String] = sheet.getRow(0).asScala.map(_.getStringCellValue).toList

      XLSXHelper.validateHeaders(headers, elektroMailingHeaders, invalidMailingPath)
    }

    "throw an error when validating invalid headers" in {
      val wb: Workbook = WorkbookFactory.create(new File(invalidMailingPath))
      val sheet = wb.getSheet("SQL Results")

      val headers: List[String] = sheet.getRow(0).asScala.map(_.getStringCellValue).toList

      an [UnexpectedHeadersException] should be thrownBy XLSXHelper.validateHeaders(headers, elektroMailingHeaders, invalidMailingPath)
    }

    "be able to tell when a cell is empty" in {
      val row: Row = sampleSheet.createRow(Faker.intBetween(0, 100))
      XLSXHelper.isRowEmpty(row) should be (true)

      row.createCell(0, CellType.BLANK)
      XLSXHelper.isRowEmpty(row) should be (true)
    }

    "be able to tell when a cell is not empty" in {
      val row: Row = sampleSheet.createRow(Faker.intBetween(0, 100))
      val cell: Cell = row.createCell(0, CellType.NUMERIC)
      cell.setCellValue(1)

      XLSXHelper.isRowEmpty(row) should be (false)
    }

    "be able to get the value of a cell properly when it's null" in {
      val cell: Cell = null
      XLSXHelper.getCellValue(cell) should be (None)
    }

    "be able to get the value of a cell properly when it's blank" in {
      val cell: Cell = sampleRow.createCell(0, CellType.BLANK)
      XLSXHelper.getCellValue(cell) should be (None)
    }

    "be able to get the value of a cell properly when it's been errored" in {
      val cell: Cell = sampleRow.createCell(0, CellType.ERROR)
      XLSXHelper.getCellValue(cell) should be (None)
    }

    "be able to get the value of a cell properly when it contains a boolean value" in {
      val cell: Cell = sampleRow.createCell(0, CellType.BOOLEAN)
      cell.setCellValue(true)
      XLSXHelper.getCellValue(cell) should be (Some(true))
    }

    "be able to get the value of a cell properly when it contains a formula" in {
      val cell: Cell = sampleRow.createCell(0, CellType.FORMULA)
      cell.setCellFormula("SUM(A1:A2)")

      XLSXHelper.getCellValue(cell) should be (Some("SUM(A1:A2)"))
    }

    "be able to get the value of a cell properly when it contains a date" in {
      var cell: Cell = null

      val cellStyleDate: CellStyle = sampleWB.createCellStyle()
      cellStyleDate.setDataFormat(sampleWB.getCreationHelper.createDataFormat().getFormat("m/d/YY h:mm"))

      val now = new Date()
      cell = sampleRow.createCell(0)
      cell.setCellStyle(cellStyleDate)
      cell.setCellValue(now)

      XLSXHelper.getCellValue(cell) should be (Some(now))
    }

    "be able to get the value of a cell properly when it contains a numeric value" in {
      val cell: Cell = sampleRow.createCell(0, CellType.NUMERIC)
      cell.setCellValue(1)

      XLSXHelper.getCellValue(cell) should be (Some(1.0))
    }

    "be able to get the value of a cell properly when it contains a string" in {
      val cell: Cell = sampleRow.createCell(0, CellType.STRING)
      cell.setCellValue("MOYLINHO")

      XLSXHelper.getCellValue(cell) should be (Some("MOYLINHO"))
    }

  }

}
