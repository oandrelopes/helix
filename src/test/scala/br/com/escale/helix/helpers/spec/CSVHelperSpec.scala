package br.com.escale.helix.helpers.spec

import java.io.File
import java.nio.file.Files

import br.com.escale.helix.helpers.CSVHelper
import br.com.escale.helix.testhelpers.Faker
import org.scalatest.{Matchers, WordSpecLike}

class CSVHelperSpec extends WordSpecLike with Matchers {

  "CSVHelper" must {

    "be able to write a valid file from a given input" in {
      val data: List[List[Any]] = List(
        List("Nome", "Idade", "E-mail"),
        List("Moylinho", 32, "moyle@email.com;moyle@escale.com.br"),
        List("Ney", 29, "ney@email.com"),
        List("Fill", 26, "fill@email.com"),
        List("LG", 30, "lg@email.com;luiz.guilherme@escale.com.br"),
        List("Rafa", 23, "rafa@email.com")
      )

      val filename: String = Faker.string + ".csv"
      CSVHelper.writeFile(filename, data)

      val file: File = new File("/tmp/" + filename)
      file should not be null

      val expected: Array[AnyRef] = Array(
        "Nome,Idade,E-mail",
        "Moylinho,32,moyle@email.com;moyle@escale.com.br",
        "Ney,29,ney@email.com",
        "Fill,26,fill@email.com",
        "LG,30,lg@email.com;luiz.guilherme@escale.com.br",
        "Rafa,23,rafa@email.com"
      )

      val actual: Array[AnyRef] = Files.lines(file.toPath).toArray()

      actual should be (expected)
    }

  }

}
